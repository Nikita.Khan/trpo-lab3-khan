﻿using System;

namespace TRPO_Lab3_Khan.Lib
{
    public class Calculator
    {
        public double GetAreaCilinder(double H, double R)
        {
            if (H <= 0 || R <= 0)
                return -1;
            
            double PI = 3.14;
            return PI * R * R + 2 * PI * R * H; 
        }
    }
}
