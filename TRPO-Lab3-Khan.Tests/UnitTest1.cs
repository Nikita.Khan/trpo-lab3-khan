using NUnit.Framework;
using System;
namespace TRPO_Lab3_Khan.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
         
            const double H = 10;
            const double R = 3;
            const double excepted = 216.66;

            var result = new Lib.Calculator().GetAreaCilinder(H, R);
            Assert.AreEqual(excepted, result,0.1);

        }
        [Test]
        public void Test2()
        {
            const double H = -1;
            const double R = -4;
            Assert.Throws<Exception>(() => new Lib.Calculator().GetAreaCilinder(H, R));
        }
        [Test]
        public void Test3()
        {
        
            const double H = 0;
            const double R = 0;
            Assert.Throws<Exception>(() => new Lib.Calculator().GetAreaCilinder(H, R));
        }
    }
}