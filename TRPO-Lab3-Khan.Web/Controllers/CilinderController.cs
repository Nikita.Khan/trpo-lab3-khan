﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TRPO_Lab3_Khan.Web.Models;
using Microsoft.AspNetCore.Http;


namespace TRPO_Lab3_Khan.Web.Controllers
{
    public class CilinderController : Controller
    {
        public IActionResult Index(IFormCollection collection)
        {
            var viewModel = new CilinderViewModel();
           
            collection.TryGetValue(nameof(CilinderViewModel.R), out var R);
            collection.TryGetValue(nameof(CilinderViewModel.H), out var H);
            collection.TryGetValue(nameof(CilinderViewModel.S), out var S);
            viewModel.R = Convert.ToDouble(R);
            viewModel.H = Convert.ToDouble(H);
            viewModel.S = new Lib.Calculator().GetAreaCilinder(Convert.ToDouble(R), Convert.ToDouble(H));
            return View(viewModel);
        }
    }
}
