﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TRPO_Lab3_Khan.wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double R = 0;
        double H = 0;
        string Result;
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        /*private void RTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            R = Convert.ToDouble(RTextBox.Text);
        }
        private double Result(double R, double H, object Label)
        {
            new Lib.Calculator().GetAreaCilinder(H, R);
        }
        private void ResultChanger(double R, double H, object Label)*/

    }
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private double _r;
        public double R
        {
            get { return _r; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Введено неверное значение (R и H > 0)");
                    return;
                }
                _r = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(R)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(T)));
            }
        }

        private double _H;
        public double H
        {
            get { return _H; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Введено неверное значение (R и H > 0)");
                    return;
                }
                _H = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(H)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(T)));

            }
        }

      
        public double T
        {
            get { return new Lib.Calculator().GetAreaCilinder(H, R); }
        }
    }
}
