﻿using System;

namespace TRPO_Lab3_Khan
{
    class Program
    {
        static void Main(string[] args)
        {
            double H,R;
            Console.WriteLine("Введите Радиус(R) и Высоту(H) цилиндра(R и H >0) :");
            Console.Write("R=");
            R = Convert.ToDouble(Console.ReadLine());
            Console.Write("H=");
            H = Convert.ToDouble(Console.ReadLine());
            if (H <= 0 || R <= 0)
            {
                Console.WriteLine("Не корректное значение!");
                return;
            }
            Console.Write("Площадь цилиндра равна = ");
            Console.Write("" + new Lib.Calculator().GetAreaCilinder(H, R) + "");
            Console.ReadKey();
        }
    }
}
